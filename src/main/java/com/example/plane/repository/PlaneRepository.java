package com.example.plane.repository;

import com.example.plane.entity.AirPlaneEntity;
import com.example.plane.entity.BrandEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlaneRepository extends JpaRepository<AirPlaneEntity, Integer> {
    List<AirPlaneEntity> findByStatus(Integer status);

}
