package com.example.plane.repository;

import com.example.plane.entity.BrandEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BrandRepository extends JpaRepository<BrandEntity, Integer> {
    List<BrandEntity> findByStatus(Integer status);
    BrandEntity findBrandEntityById(Integer brandId);

    @Query("select p.name from BrandEntity p where p.id = ?1")
    public String findNameById(Integer id);
}
