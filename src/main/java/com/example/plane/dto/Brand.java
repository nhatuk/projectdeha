package com.example.plane.dto;

import com.example.plane.entity.AirPlaneEntity;
import com.example.plane.entity.BrandEntity;

import java.util.Collection;

public class Brand {
    private Integer id;
    private String name;
    private Integer status;
    private Collection<AirPlaneEntity> airPlanesById;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Collection<AirPlaneEntity> getAirPlanesById() {
        return airPlanesById;
    }

    public void setAirPlanesById(Collection<AirPlaneEntity> airPlanesById) {
        this.airPlanesById = airPlanesById;
    }
}
