//package com.example.plane.entity;
//
//import javax.persistence.*;
//
//@Entity
//@Table(name = "user_role", schema = "flight", catalog = "")
//public class UserRoleEntity {
//    private Integer id;
//    private Integer roleId;
//    private Integer userId;
//    private RoleEntity roleByRoleId;
//    private UserEntity userByUserId;
//
//    @Id
//    @Column(name = "id", nullable = false)
//    public Integer getId() {
//        return id;
//    }
//
//    public void setId(Integer id) {
//        this.id = id;
//    }
//
//    @Basic
//    @Column(name = "role_id", nullable = true)
//    public Integer getRoleId() {
//        return roleId;
//    }
//
//    public void setRoleId(Integer roleId) {
//        this.roleId = roleId;
//    }
//
//    @Basic
//    @Column(name = "user_id", nullable = true)
//    public Integer getUserId() {
//        return userId;
//    }
//
//    public void setUserId(Integer userId) {
//        this.userId = userId;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        UserRoleEntity that = (UserRoleEntity) o;
//
//        if (id != null ? !id.equals(that.id) : that.id != null) return false;
//        if (roleId != null ? !roleId.equals(that.roleId) : that.roleId != null) return false;
//        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = id != null ? id.hashCode() : 0;
//        result = 31 * result + (roleId != null ? roleId.hashCode() : 0);
//        result = 31 * result + (userId != null ? userId.hashCode() : 0);
//        return result;
//    }
//
////    @ManyToOne
////    @JoinColumn(name = "role_id", referencedColumnName = "id")
//    public RoleEntity getRoleByRoleId() {
//        return roleByRoleId;
//    }
//
//    public void setRoleByRoleId(RoleEntity roleByRoleId) {
//        this.roleByRoleId = roleByRoleId;
//    }
//
////    @ManyToOne
////    @JoinColumn(name = "user_id", referencedColumnName = "id")
//    public UserEntity getUserByUserId() {
//        return userByUserId;
//    }
//
//    public void setUserByUserId(UserEntity userByUserId) {
//        this.userByUserId = userByUserId;
//    }
//}
