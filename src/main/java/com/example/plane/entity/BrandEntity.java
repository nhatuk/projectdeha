package com.example.plane.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "brand", schema = "flight", catalog = "")
public class BrandEntity {
    private Integer id;
    private String name;
    private Integer status;
    private List<AirPlaneEntity> airPlanesById;

    public BrandEntity  (){
        super();
    }

    public BrandEntity(Integer id, String name, Integer status, List<AirPlaneEntity> airPlanesById) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.airPlanesById = airPlanesById;
    }


    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 200)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "status", nullable = true)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BrandEntity that = (BrandEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "brandByBrandId")
    @JsonBackReference
    public List<AirPlaneEntity> getAirPlanesById() {
        return airPlanesById;
    }

    public void setAirPlanesById(List<AirPlaneEntity> airPlanesById) {
        this.airPlanesById = airPlanesById;
    }
}
