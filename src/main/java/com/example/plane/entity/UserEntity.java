//package com.example.plane.entity;
//
//import javax.management.relation.Role;
//import javax.persistence.*;
//import java.io.Serializable;
//import java.util.Collection;
//import java.util.Set;
//
//@Entity
//@Table(name = "user", schema = "flight", catalog = "")
//public class UserEntity implements Serializable {
//    private Integer id;
//    private String username;
//    private String password;
//    private String email;
//    private Integer status;
//    private Collection<UserRoleEntity> userRolesById;
//    private Set<RoleEntity> roles;
//
//    @Id
//    @Column(name = "id", nullable = false)
//    public Integer getId() {
//        return id;
//    }
//
//    public void setId(Integer id) {
//        this.id = id;
//    }
//
//    @Basic
//    @Column(name = "Username", nullable = true, length = 225)
//    public String getUsername() {
//        return username;
//    }
//
//    public void setUsername(String username) {
//        this.username = username;
//    }
//
//    @Basic
//    @Column(name = "password", nullable = true, length = -1)
//    public String getPassword() {
//        return password;
//    }
//
//    public void setPassword(String password) {
//        this.password = password;
//    }
//
//    @Basic
//    @Column(name = "email", nullable = true, length = -1)
//    public String getEmail() {
//        return email;
//    }
//
//    public void setEmail(String email) {
//        this.email = email;
//    }
//
//    @Basic
//    @Column(name = "status", nullable = true)
//    public Integer getStatus() {
//        return status;
//    }
//
//    public void setStatus(Integer status) {
//        this.status = status;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        UserEntity that = (UserEntity) o;
//
//        if (id != null ? !id.equals(that.id) : that.id != null) return false;
//        if (username != null ? !username.equals(that.username) : that.username != null) return false;
//        if (password != null ? !password.equals(that.password) : that.password != null) return false;
//        if (email != null ? !email.equals(that.email) : that.email != null) return false;
//        if (status != null ? !status.equals(that.status) : that.status != null) return false;
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = id != null ? id.hashCode() : 0;
//        result = 31 * result + (username != null ? username.hashCode() : 0);
//        result = 31 * result + (password != null ? password.hashCode() : 0);
//        result = 31 * result + (email != null ? email.hashCode() : 0);
//        result = 31 * result + (status != null ? status.hashCode() : 0);
//        return result;
//    }
//
////    @OneToMany(mappedBy = "userByUserId")
////    public Collection<UserRoleEntity> getUserRolesById() {
////        return userRolesById;
////    }
//
//    @ManyToMany
//    @JoinTable(
//            name = "user_role",
//            joinColumns = @JoinColumn(name = "user_id"),
//            inverseJoinColumns = @JoinColumn(name = "role_id")
//    )
//
////    public void setUserRolesById(Collection<UserRoleEntity> userRolesById) {
////        this.userRolesById = userRolesById;
////    }
//
//    public Set<RoleEntity> getRoles() {
//        return roles;
//    }
//
//    public void setRoles(Set<RoleEntity> roles) {
//        this.roles = roles;
//    }
//}
