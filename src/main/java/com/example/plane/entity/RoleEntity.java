//package com.example.plane.entity;
//
//import org.springframework.security.core.userdetails.User;
//
//import javax.persistence.*;
//import java.io.Serializable;
//import java.util.Collection;
//import java.util.Set;
//
//@Entity
//@Table(name = "role", schema = "flight", catalog = "")
//public class RoleEntity implements Serializable {
//    private Integer id;
//    private String name;
//    private Set<UserEntity> users;
//    private Collection<UserRoleEntity> userRolesById;
//
//    public RoleEntity(String name) {
//        this.name = name;
//    }
//
//    public RoleEntity() {
//
//    }
//
//
//    @Id
//    @Column(name = "id", nullable = false)
//    public Integer getId() {
//        return id;
//    }
//
//    public void setId(Integer id) {
//        this.id = id;
//    }
//
//    @Basic
//    @Column(name = "name", nullable = true, length = 225)
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        RoleEntity that = (RoleEntity) o;
//
//        if (id != null ? !id.equals(that.id) : that.id != null) return false;
//        if (name != null ? !name.equals(that.name) : that.name != null) return false;
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = id != null ? id.hashCode() : 0;
//        result = 31 * result + (name != null ? name.hashCode() : 0);
//        return result;
//    }
//
//    @ManyToMany(mappedBy = "roles")
////    @OneToMany(mappedBy = "roleByRoleId")
////    public Collection<UserRoleEntity> getUserRolesById() {
////        return userRolesById;
////    }
//
////    public void setUserRolesById(Collection<UserRoleEntity> userRolesById) {
////        this.userRolesById = userRolesById;
////    }
//
//    public Set<UserEntity> getUsers() {
//        return users;
//    }
//
//    public void setUsers(Set<UserEntity> users) {
//        this.users = users;
//    }
//}
