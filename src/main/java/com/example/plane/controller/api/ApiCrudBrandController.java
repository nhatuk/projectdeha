package com.example.plane.controller.api;

import com.example.plane.entity.BrandEntity;
import com.example.plane.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ApiCrudBrandController {

    @Autowired
    BrandService brandService;

    //Danh sach brand
    @GetMapping("/brand")
    public ResponseEntity<List<BrandEntity>> getAllBrand(@ModelAttribute BrandEntity brandEntity){
        List<BrandEntity> brand = brandService.getAllBrand();
        if (brand != null){
            return new ResponseEntity<>(brand, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    //Lay ra 1 brand
    @GetMapping("/brand/{id}")
    public ResponseEntity<Optional<BrandEntity>> getOneBrand(@PathVariable("id") Integer id){
        Optional<BrandEntity> brand = brandService.findById(id);
        if (brand.isPresent()){
            return new ResponseEntity<>(brand, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    //Them brand
    @PostMapping("/brand")
    public ResponseEntity<BrandEntity> addBrand(@RequestBody BrandEntity brandEntity){
        try{
            brandService.save(brandEntity);
            return new ResponseEntity<>(brandEntity, HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Sua brand
    @PutMapping("/brand/{id}")
    public ResponseEntity<Optional<BrandEntity>> editBrand(@PathVariable("id") Integer id,
                                                           @RequestBody BrandEntity brands){
        Optional<BrandEntity> brand = brandService.findById(id);
        if (brand.isPresent()){
            BrandEntity brandEntity = brand.get();

            brandEntity.setName(brands.getName());
            brandEntity.setStatus(brands.getStatus());

            brandService.save(brandEntity);
            return new ResponseEntity<>(brand, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    //Xoa brand "xoa mem"
    @DeleteMapping("/brand/{id}")
    public ResponseEntity<Optional<BrandEntity>> softDeleteBrand(@PathVariable("id") Integer id){
        Optional<BrandEntity> brand = brandService.findById(id);
        if (brand.isPresent()){
            BrandEntity brandEntity = brand.get();
            brandEntity.setStatus(0);
            brandService.save(brandEntity);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    //Khoi phuc brand
    @PutMapping("/restoreBrand/{id}")
    public ResponseEntity<Optional<BrandEntity>> restoreBrand(@PathVariable("id") Integer id){
        Optional<BrandEntity> brand = brandService.findById(id);
        if (brand.isPresent()){
            BrandEntity brandEntity = brand.get();
            brandEntity.setStatus(1);
            brandService.save(brandEntity);
            return new ResponseEntity<>(brand, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    //Xoa brand "xoa cung"
    @DeleteMapping("/deleteBrand/{id}")
    public ResponseEntity<Optional<BrandEntity>> deleteBrand(@PathVariable("id") Integer id){
        Optional<BrandEntity> brand = brandService.findById(id);
        if (brand.isPresent()){
            BrandEntity brandEntity = brand.get();
            if (brandEntity.getStatus() == 0){
                brandService.deleteById(id);
                return new ResponseEntity<>(HttpStatus.OK);
            }else {
                return new ResponseEntity<>(brand, HttpStatus.NO_CONTENT);
            }
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    //Thung rac brand "xoa mem"
    @GetMapping("/binBrand")
    public ResponseEntity<List<BrandEntity>> recycleBin(@ModelAttribute BrandEntity brandEntity){
        List<BrandEntity> brand = brandService.recycleBin();
        if (brand != null){
            return new ResponseEntity<>(brand, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
