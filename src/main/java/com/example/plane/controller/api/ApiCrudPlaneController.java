package com.example.plane.controller.api;

import com.example.plane.entity.AirPlaneEntity;
import com.example.plane.entity.BrandEntity;
import com.example.plane.repository.BrandRepository;
import com.example.plane.repository.PlaneRepository;
import com.example.plane.service.BrandService;
import com.example.plane.service.PlaneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ApiCrudPlaneController {

    @Autowired
    BrandRepository brandRepository;

    @Autowired
    PlaneRepository planeRepository;

    @Autowired
    PlaneService planeService;

    @Autowired
    BrandService brandService;

    //danh sach may bay
    @GetMapping("/plane")
    public ResponseEntity<List<AirPlaneEntity>> getAllPlane(@ModelAttribute AirPlaneEntity airPlaneEntity){
        List<AirPlaneEntity> plane = planeService.getAllPlane();
        if (plane !=null){
            return new ResponseEntity<>(plane, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    //Lay 1 may bay
    @GetMapping("/plane/{id}")
    public ResponseEntity<Optional<AirPlaneEntity>> getOnePlane(@PathVariable("id") Integer id){
        Optional<AirPlaneEntity> plane = planeService.findById(id);
        if (plane.isPresent()){
            return new ResponseEntity<>(plane, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    //them may bay
    @PostMapping("/plane/{brandId}")
    public ResponseEntity<AirPlaneEntity> addPlane(@PathVariable("brandId") Integer brandId,
                                                   @RequestBody AirPlaneEntity airPlaneEntity){
        try {
            Optional<BrandEntity> brand = brandService.findById(brandId);
            if (brand.isPresent()){
                airPlaneEntity.setBrandByBrandId(brand.get());
                airPlaneEntity.setBrandId(brandId);
                planeService.save(airPlaneEntity);
                return new ResponseEntity<>(airPlaneEntity, HttpStatus.CREATED);
            }
           return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //sua may bay
    @PutMapping("/plane/{id}")
    public ResponseEntity<Optional<AirPlaneEntity>> editPlane(@PathVariable("id") Integer id,
                                                              @RequestBody AirPlaneEntity airPlane){
        Optional<AirPlaneEntity> plane = planeService.findById(id);
        if (plane.isPresent()){
            AirPlaneEntity airPlaneEntity = plane.get();

            airPlaneEntity.setTypePlane(airPlane.getTypePlane());
            airPlaneEntity.setStatus(airPlane.getStatus());
            airPlaneEntity.setQuantity(airPlane.getQuantity());
            airPlaneEntity.setName(airPlane.getName());
            airPlaneEntity.setBrandId(airPlane.getBrandId());

            planeService.save(airPlaneEntity);
            return new ResponseEntity<>(plane,HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    //xoa may bay "xoa mem"
    @DeleteMapping("/plane/{id}")
    public ResponseEntity<Optional<AirPlaneEntity>> softDeletePlane(@PathVariable("id") Integer id){
        Optional<AirPlaneEntity> plane = planeService.findById(id);
        if (plane.isPresent()){
            AirPlaneEntity airPlaneEntity = plane.get();
            airPlaneEntity.setStatus(0);
            planeService.save(airPlaneEntity);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    //Khoi phuc may bay
    @PutMapping("/restoreBin/{id}")
    public ResponseEntity<Optional<AirPlaneEntity>> restorePlane(@PathVariable("id") Integer id){
        Optional<AirPlaneEntity> plane = planeService.findById(id);
        if (plane.isPresent()){
            AirPlaneEntity airPlaneEntity = plane.get();
            airPlaneEntity.setStatus(1);
            planeService.save(airPlaneEntity);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    //xoa may bay "xoa cung"
    @DeleteMapping("/deletePlane/{id}")
    public ResponseEntity<Optional<AirPlaneEntity>> hardDeletePlane(@PathVariable("id") Integer id){
        Optional<AirPlaneEntity> plane = planeService.findById(id);
        if (plane.isPresent()){
            AirPlaneEntity airPlaneEntity = plane.get();
            if (airPlaneEntity.getStatus() == 0){
                planeService.deleteById(id);
                return new ResponseEntity<>(HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    //danh sach may bay "xoa mem"
    @GetMapping("/binPlane")
    public ResponseEntity<List<AirPlaneEntity>> recycleBinPlane(@ModelAttribute AirPlaneEntity airPlaneEntity){
        List<AirPlaneEntity> plane = planeService.recycleBin();
        if (plane != null){
            return new ResponseEntity<>(plane, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
