package com.example.plane.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LayoutController {

    @GetMapping("/plane")
    public String customer(){
        return "customer/product";
    }

    @GetMapping("/")
    public String index(){
        return "admin/index";
    }

    @GetMapping("/product")
    public String plane(){
        return "admin/plane";
    }

//    @GetMapping("/403")
//    public String accessDenied() {
//        return "403";
//    }
//
//    @GetMapping("/login")
//    public String getLogin() {
//        return "login";
//    }
}
