package com.example.plane.service;

import com.example.plane.entity.BrandEntity;

import java.util.List;
import java.util.Optional;

public interface BrandService {
    List<BrandEntity> getAllBrand();
    Optional<BrandEntity> findById(Integer id);
    BrandEntity save(BrandEntity brand);
    List<BrandEntity> recycleBin();
    public void deleteById(Integer id);
    String findNameById(Integer id);
}
