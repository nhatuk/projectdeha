package com.example.plane.service;

import com.example.plane.entity.AirPlaneEntity;

import java.util.List;
import java.util.Optional;

public interface PlaneService {
    List<AirPlaneEntity> getAllPlane();
    Optional<AirPlaneEntity> findById(Integer id);
    AirPlaneEntity save(AirPlaneEntity plane);
    List<AirPlaneEntity> recycleBin();
    public void deleteById(Integer id);
}
