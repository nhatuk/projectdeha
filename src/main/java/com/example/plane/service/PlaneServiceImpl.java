package com.example.plane.service;

import com.example.plane.entity.AirPlaneEntity;
import com.example.plane.repository.PlaneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PlaneServiceImpl implements PlaneService{

    @Autowired
    PlaneRepository planeRepository;

    @Override
    public List<AirPlaneEntity> getAllPlane() {
        List<AirPlaneEntity> air = planeRepository.findByStatus(1);
        return air;
    }

    @Override
    public Optional<AirPlaneEntity> findById(Integer id) {
        return planeRepository.findById(id);
    }

    @Override
    public AirPlaneEntity save(AirPlaneEntity plane) {
        return planeRepository.save(plane);
    }

    @Override
    public List<AirPlaneEntity> recycleBin() {
        List<AirPlaneEntity> airs = planeRepository.findByStatus(0);
        return airs;
    }

    @Override
    public void deleteById(Integer id) {
        planeRepository.deleteById(id);
    }
}
