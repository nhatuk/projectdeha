package com.example.plane.service;

import com.example.plane.entity.BrandEntity;
import com.example.plane.repository.BrandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BrandServiceImpl implements BrandService {

    @Autowired
    BrandRepository brandRepository;

    @Override
    public List<BrandEntity> getAllBrand() {
        List<BrandEntity> brand = brandRepository.findByStatus(1);
        return brand;
    }

    @Override
    public Optional<BrandEntity> findById(Integer id) {
        return brandRepository.findById(id);
    }

    @Override
    public BrandEntity save(BrandEntity brand) {
        return brandRepository.save(brand);
    }

    @Override
    public List<BrandEntity> recycleBin() {
        List<BrandEntity> brands = brandRepository.findByStatus(0);
        return brands;
    }

    @Override
    public void deleteById(Integer id) {
        brandRepository.deleteById(id);
    }

    @Override
    public String findNameById(Integer id) {
        return brandRepository.findNameById(id);
    }
}
