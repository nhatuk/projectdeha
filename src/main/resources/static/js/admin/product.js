// function getProductRecycle(){
//     $.ajax({
//         url: "api/recycle",
//         method: "GET",
//         dataType: "json",
//         success: function (data){
//             var recycle = $("#tbRecycle");
//             recycle.empty();
//             $(data).each(function (index, element){
//                 recycle.append('<tr><td><button type="button" onclick="restore('+element.id+')" class="btn btn-primary">'+element.id+'</button></td>' +
//                     '<td>' + element.name + '</td>' +
//                     '<td>' + element.price + '</td>' +
//                     '<td>' + element.qty + '</td>' +
//                     '<td><img style="max-width: 80px;max-height: 80px;" src="' + element.image + '"></td>' +
//                     '<td>' + element.description + '</td>' +
//                     '<td>' + element.status + '</td>' +
//                     '<td><button onclick="hardRemove('+element.id+')" type="button" class="btn btn-danger">Delete</button></td></tr>')
//             });
//         },
//         error:function (error) {
//             alert(error);
//         }
//     });
// }
// getProductRecycle();
//
// function getId(id){
//     $.ajax({
//         url: 'api/product/'+id,
//         method: 'GET',
//         dataType: 'json',
//         success: function (data){
//             $('#pId').val(data.id);
//             $('#name').val(data.name);
//             $('#description').val(data.description);
//             $('#status').val(data.status);
//             $('#price').val(data.price);
//             $('#image').val(data.image);
//             $('#qty').val(data.qty);
//         },
//         error: function (error){
//             alert(error);
//         }
//     });
// }
//
// var product = {};
// var url = "";
// var method = "";
// $('#BtnAddProduct').click(function (){
//     product.name = $('#name').val();
//     product.description = $('#description').val();
//     product.image = $('#image').val();
//     product.status = $('#status').val();
//     product.price = $('#price').val();
//     product.qty = $('#qty').val();
//     var id1 = $('#pId').val();
//     if (id1){
//         url = "api/product/" +id1;
//         method = "PUT";
//         console.log(id1);
//     }else {
//         url = "api/product";
//         method = "POST";
//     }
//     var productObj = JSON.stringify(product);
//     $.ajax({
//         url : url,
//         method : method,
//         data : productObj,
//         contentType : 'application/json; charset=utf-8',
//         success : function () {
//             // alert('function successfully')
//             $("#exampleModalLong").modal('hide');
//             // location.reload();
//             reset();
//         },
//         error: function (error){
//             alert(error);
//         }
//     });
// })
//
// function restore(id){
//     swal({
//         title: "Bạn có muốn khôi phục tệp tin mã: " +id,
//         text: "Tệp tin " +id+ " sau khi khôi phục sẽ quay lại mặc định",
//         type: "warning",
//         showCancelButton: true,
//         confirmButtonText: "Khôi phục sản phẩm!",
//         cancelButtonText: "Không khôi phục!",
//         closeOnConfirm: false,
//         closeOnCancel: false,
//     },function (isConfirm){
//         if (isConfirm){
//             $.ajax({
//                 url: 'api/restore/'+id,
//                 method: 'PUT',
//                 dataType: 'json',
//                 success: function (){
//                     swal("Đã khôi phục", "Mã : " +id+ " đã được khôi phục" , "success");
//                 },
//                 error: function (error){
//                     swal("Có lỗi!", "Vui lòng kiểm tra lại code", "error");
//                 }
//
//             });
//         }else {
//             swal("Không khôi phục!", "Mã : " +id+ " không được khôi phục", "error");
//         }
//     });
// }
//
// function remove(id){
//     swal({
//         title: "Bạn có chắc muốn xoá",
//         text: "Thư mục sau khi xoá sẽ ở trong thùng rác",
//         type: "warning",
//         showCancelButton: true,
//         confirmButtonText: "Xoá sản phẩm!",
//         cancelButtonText: "Không xoá!",
//         closeOnConfirm: false,
//         closeOnCancel: false,
//     },function (isConfirm){
//         if (isConfirm){
//             $.ajax({
//                 url: 'api/product/'+id,
//                 method: 'delete',
//                 dataType: 'json',
//                 success: function (){
//                     swal("Đã xoá", "Sản phẩm đã chuyển vào thùng rác", "success");
//                     var tbl = document.getElementById("tbAdmin");
//                     var rows = tbl.rows.length;
//                     for (var i=0;i<rows;i++){
//                         var tr = tbl.rows[i];
//                     }
//                     document.getElementById("tbAdmin").deleteRow(tr);
//                     console.log(tr);
//                 },
//                 error: function (error){
//                     swal("Có lỗi!", "Vui lòng kiểm tra lại code", "error");
//                 }
//
//             });
//         }else {
//             swal("Không xoá!", "Sản phẩm vẫn còn nguyên", "error");
//         }
//     });
// }
//
// function hardRemove(id){
//     var tbl = document.getElementById("tbRecycle");
//     var rows = tbl.rows.length;
//     for (var i=0;i<rows;i++){
//         var tr = tbl.rows[i];
//     }
//     console.log(tr);
//     swal({
//         title: "Bạn có chắc muốn xoá",
//         text: "Thư mục sau khi xoá sẽ biến mất vĩnh viễn",
//         type: "warning",
//         showCancelButton: true,
//         confirmButtonText: "Xoá sản phẩm!",
//         cancelButtonText: "Không xoá!",
//         closeOnConfirm: false,
//         closeOnCancel: false,
//     },function (isConfirm){
//         if (isConfirm){
//             $.ajax({
//                 url: 'api/delete/'+id,
//                 method: 'delete',
//                 dataType: 'json',
//                 success: function (){
//                     swal("Đã xoá", "Sản phẩm đã được xoá cứng", "success");
//                     document.getElementById("tbRecycle").deleteRow(tr);
//
//                 },
//                 error: function (error){
//                     swal("Có lỗi!", "Vui lòng kiểm tra lại code", "error");
//                 }
//
//             });
//         }else {
//             swal("Không xoá!", "Sản phẩm vẫn ở trong thùng rác", "error");
//         }
//     });
// }
//
//
//
// function reset(){
//     $('#name').val('')
//     $('#description').val('')
//     $('#status').val('')
//     $('#price').val('')
//     $('#qty').val('')
//     $('#image').val('')
// }